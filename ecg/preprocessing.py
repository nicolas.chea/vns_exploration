import biosppy.signals.ecg as bsp_ecg
import numpy as np


def detect_qrs_hamilton(ecg_data, fs):
    qrs_frames = []
    try:
        qrs_frames = bsp_ecg.ecg(
            signal=np.array(ecg_data), sampling_rate=fs, show=False)[2]
    except Exception:
        print("Exception in detect_qrs_gqrs")
    return qrs_frames


def extract_rr_intervals(frame_data, fs):
    rr_intervals = np.zeros(len(frame_data) - 1)
    for i in range(0, (len(frame_data) - 1)):
        rr_intervals[i] = (frame_data[i + 1] - frame_data[i]) * 1000.0 / fs

    return rr_intervals


def get_cardiac_infos(ecg_data, sampling_frequency):
    qrs_frames = detect_qrs_hamilton(ecg_data, sampling_frequency)

    rr_intervals = np.zeros(0)
    if len(qrs_frames):
        rr_intervals = extract_rr_intervals(
            qrs_frames, sampling_frequency)
    return qrs_frames, rr_intervals


def get_cardiac_frequency(df, fs):
    qrs_frames, rr_intervals = get_cardiac_infos(df["ECG1+ECG1-"], fs)
    df_detections = df[["ECG1+ECG1-"]]
    df_detections = df_detections.iloc[qrs_frames[:-1]]
    df_detections['rr_interval'] = rr_intervals/1000
    df_detections['bpm'] = 60/df_detections['rr_interval']
    df_detections.drop(columns='ECG1+ECG1-', inplace=True)
    return df_detections