import pandas as pd 

def get_vns_interval_by_std(df, fs, interval_min_sec, threshold, threshold_min):
    list_group = []
    
    # we look for consecutive samples > threshold with a number of samples > fixed time 
    for key, group in df[df["EMG1+EMG1-"]>threshold].groupby((df["EMG1+EMG1-"] < threshold).cumsum()):
        if len(group) > round(fs*interval_min_sec):
            list_group.append(group)
            
    # if we don't find a large enough group, we decrease the threshold progressively
    threshold_step = threshold
    if len(list_group) == 0 : 
        number_steps = int((threshold - threshold_min)*2)
        for i in range(number_steps):
            threshold_step = threshold - (i+1)*0.5
            for key, group in df[df["EMG1+EMG1-"]>threshold_step].groupby((df["EMG1+EMG1-"] < threshold_step).cumsum()):
                if len(group) > round(fs*interval_min_sec):
                    list_group.append(group)
            if len(list_group) > 0:
                break
                
    return list_group, threshold_step

def get_vns_interval_by_std_total(df, fs, interval_min_sec, threshold):
    list_group_begin = []
    list_group_end = []
    
    # we look for consecutive samples > threshold with a number of samples > fixed time 
    for key, group in df[df["EMG1+EMG1-"]>threshold].groupby((df["EMG1+EMG1-"] < threshold).cumsum()):
        if len(group) > round(fs*interval_min_sec) :
            list_group_begin.append(group.iloc[0].name)
            list_group_end.append(group.iloc[-1].name)
                
    return list_group_begin, list_group_end

def correct_vns_time_interval(df, index, timedelta, by="begin"):
    if by == "begin" :
        df.at[index, 'end'] = df["begin"].iloc[index] + timedelta
    else : 
        df.at[index, 'begin'] = df["end"].iloc[index] - timedelta
        
def manual_interval_correction(df, index, begin, end):
    df["begin"].iloc[index] = pd.Timestamp(begin)
    df["end"].iloc[index] = pd.Timestamp(end)
    df["duration"].iloc[index] = (df["end"].iloc[index] - df["begin"].iloc[index]).total_seconds()