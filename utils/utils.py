import os
import mne
import pandas as pd
import re
import datetime


def extract_numbers_from_list(l):
    return [re.sub("[^0-9]", "", i) for i in l]


def remove_list_from_list(list_items, list_items_to_remove):
    for element in list_items_to_remove:
        if element in list_items:
            list_items.remove(element)
    return list_items


def extract_time_window_event(df, 
                              begin_event, 
                              start_df, 
                              sample_interval=3.90625, 
                              end_event=None, 
                              time_before_event=30,
                              time_after_event=30,
                              time_column_name="Date",
                              vns=False,
                             ):
    '''
    From an annotation DataFrame, extract the selected part using the sample number of the event
    '''
    begin_event_time = pd.to_datetime(df.iloc[[begin_event]][time_column_name]).iloc[0]
    # Annotation file
    if not vns : 
        end_event_time = pd.to_datetime(df.iloc[[end_event]][time_column_name]).iloc[0]
    # VNS file
    else :
        end_event_time = begin_event_time + datetime.timedelta(seconds=60)
    
    begin_event_time_from_start = begin_event_time - start_df
    end_event_time_from_start = end_event_time - start_df
    event_window_begin = begin_event_time_from_start - datetime.timedelta(seconds=time_before_event)
    event_window_end = end_event_time_from_start + datetime.timedelta(seconds=time_after_event)
    event_window_begin_ms = round((event_window_begin.total_seconds() * 1000)/sample_interval)
    event_window_end_ms = round((event_window_end.total_seconds() * 1000)/sample_interval)
    
    return event_window_begin_ms, event_window_end_ms, begin_event_time,  end_event_time

def print_full(df):
    pd.set_option('display.max_rows', len(df))
    print(df)
    pd.reset_option('display.max_rows')
    
    




