import os
import mne
import pandas as pd
import re
import datetime
from tqdm.notebook import tqdm
from .utils import extract_numbers_from_list

def create_df_from_file(folder, file, signal=False, start_time=1, stop_time=1000, exclude=[]):
    data_file = os.path.join(folder, file)
    if signal:
        try :
            if exclude:
                data = mne.io.read_raw_edf(data_file, exclude=exclude)
            else:
                data = mne.io.read_raw_edf(data_file)
        except Exception:
            print("Cannot read the edf file")
        df = data.to_data_frame(time_format="datetime", start=start_time, stop=stop_time)
        return df, data
    else:
        df = pd.read_csv(data_file, sep="|", encoding='iso-8859-1')
        return df

def create_annotation_signal_segment(patient_data_folder, 
                                  session_number, 
                                  segment_number, 
                                  start_time=1, 
                                  stop_time=100000, 
                                  exclude=[],
                                    ):
    '''
    Create a DataFrame for each of the annotation/signal/segment file
    '''
    annotation_file = "Annotations_EEG_" + str(session_number) + ".csv"
    signal_file = "EEG_" + str(session_number) + "_s" + str(segment_number) + ".edf"
    segment_file = "Segments_EEG_" + str(session_number) + ".csv"
    df_annotation = create_df_from_file(patient_data_folder, annotation_file)
    df_signal, raw_signal = create_df_from_file(patient_data_folder, signal_file, signal=True, start_time=start_time, stop_time=stop_time, exclude=exclude)
    df_segment = create_df_from_file(patient_data_folder, segment_file)
    
    return df_annotation, df_signal, df_segment, raw_signal

def extract_vns_data(filename):
    try:    
        df = pd.read_excel(filename, 
                        engine='openpyxl',
                        header=7,
                        skipfooter=3,
                        sheet_name="Start Here",
                        index_col=0,
                        usecols="A:E")
    except Exception:
        print("Cannot read the excel file")
    df.columns = df.columns.str.replace(' ','')
    df["Time"] = df["Date"] + "T" + df["TimeStamp"]
    df["Time"] = df["Time"].str.replace(' ','') 
    df.drop(["Date", "TimeStamp"], axis=1, inplace=True)
    return df


def extract_sessions_numbers(patient_data_folder):
    list_sessions = [i for i in os.listdir(patient_data_folder) if i.startswith('Segments_')]
    list_sessions_numbers = extract_numbers_from_list(list_sessions)
    return list_sessions_numbers


def extract_sessions_patient(patient_data_folder, list_sessions_numbers, list_exclude, segment_number=1):
    dict_dfs = {}
    
    for session in tqdm(list_sessions_numbers):
        signal_file = "EEG_" + str(session) + "_s" + str(segment_number) + ".edf"
        df_annotation, df_signal, df_segment, raw = create_annotation_signal_segment(patient_data_folder, 
                                                                      session_number=session, 
                                                                      segment_number=segment_number, 
                                                                      start_time=None, 
                                                                      stop_time=None,
                                                                      exclude=list_exclude)
        number_samples = len(raw.get_data()[0])
        fs = raw.info['sfreq']
        sample_interval = (1/fs)*1000
        start_df = pd.to_datetime(df_signal.iloc[[0]]["time"].iloc[0])
        dict_dfs[session] = [df_annotation, df_signal, df_segment, number_samples, fs, sample_interval, start_df, signal_file]
    return dict_dfs


def df_from_vns_data(list_sessions_numbers, dict_dfs):
    list_df_vns = []
    list_df_vns_y = []
    list_df_vns_n = []
    for session in list_sessions_numbers:
        df_segment = dict_dfs[session][2]
        signal_file = dict_dfs[session][-1]
        start_df = dict_dfs[session][-2]
        start_date = df_segment["Start-date"].iloc[0]
        end_date = df_segment["End-date"].iloc[0]
        df_vns = extract_vns_data("data_vns/PAT_9_06_11_au_19_11_2021.xlsm")
        df_vns['TherapyDeliveredY/N'] = df_vns['TherapyDeliveredY/N'].str.replace(' ','') 
        segment_mask = (df_vns['Time'] > start_date) & (df_vns['Time'] <= end_date)
        df_vns = df_vns.loc[segment_mask]
        df_vns["session"] = session
        df_vns["file"] = signal_file
        df_vns["start_df"] = start_df
        df_vns_y = df_vns.loc[df_vns['TherapyDeliveredY/N'] == 'Y']
        df_vns_n = df_vns.loc[df_vns['TherapyDeliveredY/N'] == 'N']
        list_df_vns.append(df_vns)
        list_df_vns_y.append(df_vns_y)
        list_df_vns_n.append(df_vns_n)
    df_vns_total = pd.concat(list_df_vns)
    df_vns_y_total = pd.concat(list_df_vns_y)
    df_vns_y_total["Time"] = pd.to_datetime(df_vns_y_total["Time"])
    df_vns_y_total["Time"] = df_vns_y_total["Time"].dt.tz_localize("GMT")
    df_vns_y_total = df_vns_y_total.reset_index()
    df_vns_n_total = pd.concat(list_df_vns_n)