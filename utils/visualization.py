import matplotlib.pyplot as plt
import seaborn as sns
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots

def correlation_heatmap_from_df(df, list_channels, title='Corrélation entre éléctrodes', cmap="magma"):
    df_corr = df[list_channels].corr()
    plt.figure(figsize=(16, 10))
    heatmap = sns.heatmap(df_corr, vmin=-1, vmax=1, cmap= "magma")
    heatmap.set_title(title, fontdict={'fontsize':12});
    
def plot_event(fig, 
               df, 
               y_label, 
               begin_event, 
               end_event, 
               short_rolling_window="10s", 
               color="red",
               x_label="time", 
               name_event="crise",
               row=None, 
               col=None, 
               long_window=False,
               std=True,
               short_mean=True,
              ):
    
    fig.add_trace(go.Scatter(x=df.index, y=df[y_label],
                    mode='lines',
                    name=y_label),
    row=row, col=col)
    
    fig.add_vrect(x0=begin_event, 
              x1=end_event, line_width=0, fillcolor=color, opacity=0.2, annotation_text=name_event,
    row=row, col=col)
    
    if short_mean : 
        fig.add_trace(go.Scatter(x=df.index, y=df[y_label].rolling(short_rolling_window).mean(),
                        mode='lines',
                        name='Moyenne glissante courte '+ y_label),
        row=row, col=col)
    if std :
        fig.add_trace(go.Scatter(x=df.index, y=df[y_label].rolling("1s").std(),
                        mode='lines',
                        name='std sur fenêtre de 1s '+ y_label),
        row=row, col=col)
    
    if long_window : 
        fig.add_trace(go.Scatter(x=df.index, y=df[y_label].rolling("5T").mean(),
                    mode='lines',
                    name='Moyenne glissante sur 5mins '+ y_label),
        row=row, col=col)

        

def plot_events(list_channels, 
                df_event_interval, 
                begin_event, 
                end_event, 
                name_event="crise", 
                short_rolling_window="10s", 
                long_window=False):
    fig = go.Figure()
    fig = make_subplots(rows=len(list_channels), cols=1)
    for i, channel in enumerate(list_channels):
        count = i + 1
        plot_event(fig, df_event_interval, y_label=channel,
            begin_event=begin_event, end_event=end_event, row=count, long_window=long_window, col=1, name_event=name_event, short_rolling_window=short_rolling_window)
    fig.show()
    
